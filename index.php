<?php
require_once __DIR__ . '/vendor/autoload.php';

function getDateId(){
    $file = "id.txt";

    if(!file_exists($file)){
        $myfile = fopen("id.txt", "w") or die("Unable to open file!");
        $last_date = "2022-09-04 13:32:01.427";
        fwrite($myfile, $last_date);
        fclose($myfile);
        return  $last_date;
    }

    $myfile = fopen("id.txt", "r") or die("Unable to open file!");
    $last_date = fread($myfile,filesize("id.txt"));
    return $last_date;
}

function setDateId($date){
    $file = "id.txt";
    $myfile = fopen($file, "w") or die("Unable to open file!");
    fwrite($myfile, $date);
    fclose($myfile);
}

function getDbData(){
    $data = [];
    $serverName = "PAZXGTYV8V"; //serverName\instanceName
    $connectionInfo = array( "Database"=>"master", "UID"=>"beclick", "PWD"=>"Beclick1234567");
    $conn = sqlsrv_connect( $serverName, $connectionInfo);
    $last_date = getDateId();
    if( $conn ) {
        echo "Connection established.<br />";
        $sql = "SELECT TOP (1000) [CMUId]
      ,[Lat]
      ,[Long]
      ,[GPSDateTime]
      ,[LastGPSFix]
      ,[UTCTime]
      ,[GWTime]
      ,[InsertDate]
      ,[TRId]
	 
        FROM [CellocatorHub].[dbo].[UplinkMsgLog]
        

  order by GPSDateTime desc";
        $params = array();
        $options =array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
        $stmt = sqlsrv_query( $conn, $sql , $params, $options );
        $row_count = sqlsrv_num_rows( $stmt );
        if ($row_count === false){
            die("Not row");
        }else{
            echo "bien";
        }
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            $data[] = $row;
        }
        sqlsrv_close($conn);
    }else{
        echo "Connection could not be established.<br />";
        die( print_r( sqlsrv_errors(), true));
    }
    return $data;
}
function convertDate($data){
    $new_data = [];
    $date_time_colum = [
        "GPSDateTime",
        "LastGPSFix",
        "UTCTime",
        "GWTime",
        "InsertDate"
    ];
    $integer_columns = [
        "CMUId",
        "Lat",
        "Long",
        "TRId"
    ];
    foreach ($data as $item){
        $new_item = [];
        foreach ($date_time_colum as $column){
            $new_item[$column] = $item[$column]->getTimestamp();
        }
        foreach ($integer_columns as $column){
            $new_item[$column] = $item[$column];
        }
        $new_data[] = $new_item;
    }
    return $new_data;
}
$data = getDbData();
//print_r($data);
if(count($data)>0){
    $data = convertDate($data);
}

use Morrislaptop\Firestore\Factory;
use Kreait\Firebase\ServiceAccount;

// This assumes that you have placed the Firebase credentials in the same directory
// as this PHP file.
$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/firebase_credentials.json');

$firestore = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->createFirestore();

$collection = $firestore->collection('chips');
if(count($data)>0){
    foreach ($data as $value){
        $user = $collection->document(md5($value["GPSDateTime"]));
        $user->set($value);
    }
}


$user = $collection->document('yarbek2');
$user->set(['name'=>'123','age'=>123]);

